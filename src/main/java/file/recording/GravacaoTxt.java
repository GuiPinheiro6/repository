package file.recording;

import model.Relatorio;
import java.io.FileWriter;
import java.io.IOException;

public class GravacaoTxt {

    public void gravacaoArquivoTxt(Relatorio lerArquivo) throws IOException {
        FileWriter arquivo = new FileWriter("C:\\data\\out\\relatorio de vendas.txt");
        arquivo.write(formatacaoSaidaTxt(lerArquivo));
        arquivo.close();
    }

    private String formatacaoSaidaTxt(Relatorio lerArquivo) {
        return "Quantidade de Clientes: " + lerArquivo.getQtdClientes() + "\n" + "Quantidade de Vendedores: "
                + lerArquivo.getQtdVendedores() + "\n" + "ID da venda mais cara: " + lerArquivo.getIdVenda() + "\n"
                + "O pior vendedor: " + lerArquivo.getPiorVendedor();
    }
}

