package file.reading;

import lombok.NoArgsConstructor;
import model.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;



@NoArgsConstructor
public class LeituraTxt {

    private List<Vendedor> listaVendedor = new ArrayList<>();

    private List<Cliente> listaCliente = new ArrayList<>();

    private List<Venda> listaVenda = new ArrayList<>();

    private List<Item> listaItensTotais = new ArrayList<>();

    public List<String> ArquivoTxt() {
        File pastaArquivo = new File("C:\\data\\in\\");
        String[] listaDeNomes = pastaArquivo.list();
        List<String> nomesArquivos = new ArrayList<>();
        for (String nome : listaDeNomes) {
            if (nome.contains(".txt")) {
                nomesArquivos.add(nome);
            }
        }
        return nomesArquivos;
    }

    public Relatorio lerArquivo(String nomeArquivo) throws IOException {
        Path path = Paths.get("C:\\data\\in\\" + nomeArquivo);

        Files.lines(path).forEach(linha -> {
            String identificacao = linha.substring(0, 3);

            if (identificacao.equals("001")) {
                String[] separacao = linha.split("ç");
                criacaoVendedor(separacao);
            } else if (identificacao.equals("002")) {
               String[] separacao = linha.split("ç");
                criacaoCliente(separacao);
            } else if (identificacao.equals("003")) {
                String[] separacao = linha.split("ç");
                criacaoVenda(separacao);
            } else {
                throw new RuntimeException("Erro");
            }
        });

        return criacaoDoRelatorio();
    }

    private int vendaMaisCara(List<Venda> vendas) {


    }

    private String piorVendedor(List<Venda> vendas) {

    }

    private void criacaoVendedor(String[] separacao) {
        Vendedor vendedor = new Vendedor();

    }

    private void criacaoCliente(String[] separacao) {
        Cliente cliente = new Cliente();

    }

    private void criacaoVenda(String[] separacao) {
        Venda venda = new Venda();

    }

    private Relatorio criacaoDoRelatorio() {
        Relatorio relatorio = new Relatorio();

        relatorio.setQtdClientes(listaCliente.size());
        relatorio.setQtdVendedores(listaVendedor.size());
        relatorio.setIdVenda(vendaMaisCara(listaVenda));
        relatorio.setPiorVendedor(piorVendedor(listaVenda));

        return relatorio;
    }

}





