package model;

public class Relatorio {

    private int qtdClientes;
    private int qtdVendedores;
    private int idVenda;
    private String piorVendedor;

    public int getQtdClientes() {
        return qtdClientes;
    }

    public void setQtdClientes(int qtdClientes) {
        this.qtdClientes = qtdClientes;
    }

    public int getQtdVendedores() {
        return qtdVendedores;
    }

    public void setQtdVendedores(int qtdVendedores) {
        this.qtdVendedores = qtdVendedores;
    }

    public int getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(int idVenda) {
        this.idVenda = idVenda;
    }

    public String getPiorVendedor() {
        return piorVendedor;
    }

    public void setPiorVendedor(String piorVendedor) {
        this.piorVendedor = piorVendedor;
    }
}
