package model;

import java.util.List;

public class Venda {

    private int id;
    private String nome;
    private Double vendaTotal;
    private List<Item> item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public Double getVendaTotal() {
        return vendaTotal;
    }

    public void setVendaTotal(Double vendaTotal) {
        this.vendaTotal = vendaTotal;
    }
}
