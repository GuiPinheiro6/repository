package validation;

import model.Cliente;
import model.Item;
import model.Venda;
import model.Vendedor;
import org.hibernate.validator.internal.constraintvalidators.hv.br.CNPJValidator;
import org.hibernate.validator.internal.constraintvalidators.hv.br.CPFValidator;

public class Validacao {

    public static boolean validaCamposVendedor(Vendedor vendedor) {
        validaCPF(vendedor.getCpf());
        if (vendedor.getCpf() == null) {
            if (vendedor.getCpf().isBlank()) {
                throw new RuntimeException("Cpf incorreto");
            }
        }
        if (vendedor.getNome() == null) {
            if (vendedor.getNome().isBlank()) {
                throw new RuntimeException("Nome incorreto");
            }
        }
        if (vendedor.getSalario() == null) {
            throw new RuntimeException("Salario incorreto");
        }
        return true;
    }

    public static boolean validaCamposCliente(Cliente cliente) {
        validaCNPJ(cliente.getCnpj());
        if (cliente.getCnpj() == null) {
            if (cliente.getCnpj().isBlank()) {
                throw new RuntimeException("Cnpj incorreto");
            }
        }
        if (cliente.getNome() == null) {
            if (cliente.getNome().isBlank()) {
                throw new RuntimeException("Nome incorreto");
            }
        }
        if (cliente.getAreaDeNegocio() == null) {
            if (cliente.getAreaDeNegocio().isBlank()) {
                throw new RuntimeException("Business area incorreto");
            }
        }
        return true;
    }

    public static boolean validaCamposVenda(Venda venda) {
        if (venda.getId() == 0) {
            throw new RuntimeException("Id venda incorreto");
        }
        if (venda.getNome() == null) {
            if (venda.getNome().isBlank()) {
                throw new RuntimeException("Nome incorreto");
            }
        }
        return true;
    }

    public static boolean validaCamposItem(Item item) {
        if (item.getId() == 0) {
            throw new RuntimeException("Id item incorreto");
        }
        if (item.getQuantidade() == 0) {
            throw new RuntimeException("Quantidade incorreta");
        }
        if (item.getPreco() == null) {
            throw new RuntimeException("Preço incorreto");
        }
        return true;
    }

    private static void validaCPF(String cpf) {
        CPFValidator cpfValidator = new CPFValidator();
        if (cpf == null) {
            if (cpf.isBlank()) {
                throw new RuntimeException("Cpf incorreto");
            }
        }
    }

    private static void validaCNPJ(String cnpj) {
        CNPJValidator cnpjValidator = new CNPJValidator();
        if (cnpj == null) {
            if (cnpj.isBlank()) {
                throw new RuntimeException("Cnpj incorreto");
            }
        }
    }
}



